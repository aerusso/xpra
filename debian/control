Source: xpra
Section: x11
Priority: optional
Standards-Version: 4.3.0
Maintainer: Dmitry Smirnov <onlyjob@debian.org>
Uploaders: أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
Build-Depends: debhelper (>= 11~) ,pkg-config ,dh-python
    ,cython (>= 0.19)
    ,libavcodec-dev (>= 7:3.1.3~)
    ,libavformat-dev (>= 7:3.1.3~)
    ,libavutil-dev (>= 7:3.1.3~)
    ,libswscale-dev (>= 7:3.1.3~)
    ,libvpx-dev (>= 1.6.0~)
    ,libpam0g-dev
    ,libsystemd-dev
    ,libturbojpeg0-dev
    ,libwebp-dev
    ,libx11-dev
    ,libx264-dev
    ,libx265-dev
    ,libxcomposite-dev
    ,libxdamage-dev
    ,libxkbfile-dev
    ,libxtst-dev
    ,python-all-dev
    ,python-gi-dev
    ,python-gtk2-dev
Homepage: http://xpra.org/
Vcs-Git: https://salsa.debian.org/debian/xpra.git
Vcs-Browser: https://salsa.debian.org/debian/xpra

Package: xpra
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}
    ,adduser
    ,python-gi-cairo
    ,python-gtk2
    ,x11-xserver-utils
    ,xserver-xorg-input-void
    ,xserver-xorg-video-dummy
# Packet Encoding (http://xpra.org/trac/wiki/PacketEncoding):
    ,python-rencode
Recommends: ${misc:Recommends}
    ,keyboard-configuration
# SSH:
    ,python-paramiko
    ,openssh-client
    ,ssh-askpass
# Packet Compression (http://xpra.org/trac/wiki/PacketEncoding):
    ,python-lz4
    ,python-lzo
# PNG [png,png/L,png/P], JPEG and WebP (python-pil) support:
    ,python-pil | python-imaging
# OpenGL support:
    ,python-gtkglext1
# Notifications forwarding:
    ,python-dbus
# usr/lib/cups/backend/xpraforwarder
    ,python-uritools
Suggests: openssh-server
# OpenCL acceleration:
    ,python-pyopencl
# Audio:
#            [vorbis]
    ,gstreamer1.0-plugins-base
#   [wavpack, wav, flac, speex]          [mp3]                       [opus]
    ,gstreamer1.0-plugins-good | gstreamer1.0-plugins-ugly | gstreamer1.0-plugins-bad
    ,python-gst-1.0
    ,pulseaudio
    ,pulseaudio-utils
# For publishing servers via mDNS:
    ,python-avahi
    ,python-netifaces
# Printer forwarding:
    ,cups-client
    ,cups-common
    ,cups-filters
    ,cups-pdf
    ,python-cups
# Webcam forwarding:
    ,python-opencv
    ,v4l2loopback-dkms
# Only useful when connecting using a JSON / YAML only client:
    ,python-yaml
# misc accel.:
    ,python-uinput
# Nvidia NVENC support:
#    ,python-pycuda
#    ,libnvidia-encode1
# HTML5 support:
#    ,websockify
# python-appindicator is not useful on Debian? Ubuntu only?
#    ,python-appindicator
Description: tool to detach/reattach running X programs
 Xpra gives you the functionality of GNU Screen for X applications.
 .
 It allows the user to view remote X applications on their local machine, and
 disconnect and reconnect from the remote machine without losing the state of
 the running applications.
 .
 Unlike VNC, these applications are "rootless".  They appear as individual
 windows inside your window manager rather than being contained within a single
 window.
